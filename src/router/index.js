import Vue from 'vue';
import VueRouter from 'vue-router';
import goods from '../components/tab/goods/goods';
import seller from '../components/tab/seller/seller';
import ratings from '../components/tab/ratings/ratings';

Vue.use(VueRouter);
Vue.config.productionTip = false;

const routers = [
  {path: '/', redirect: '/goods'},
  {path: '/goods', component: goods},
  {path: '/seller', component: seller},
  {path: '/ratings', component: ratings}
];
const router = new VueRouter({
  linkActiveClass: 'active', // 添加激活时的默认样式
  // mode: 'history', // 使用history模式不会有#
  routes: routers // 这个键必须是route!!!否则router-view不会显示！！！！
});

export default router;
